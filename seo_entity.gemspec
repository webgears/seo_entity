$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "seo_entity/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "seo_entity"
  s.version     = SeoEntity::VERSION
  s.authors     = ["Marina Starostina"]
  s.email       = ["starostinamb@gmail.com"]
  s.homepage    = "https://bitbucket.org/mstarostina/seo_entity"
  s.summary     = "Creates Seo Entity on top of Active Admin"
  s.description = "Creates Seo Entity with interface in Active Admin for handling seo variables like head, page title and so on"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", ">= 4.0.0"
end
