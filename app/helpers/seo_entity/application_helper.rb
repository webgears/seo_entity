module SeoEntity
  module ApplicationHelper

    include ActionView::Helpers::SanitizeHelper

    def seo_entity
      url = request.fullpath
      black_list = %w(/404 /500 /422)
      url_wo_query = url.split('?').first
      if black_list.include?(url_wo_query)
        @seo_entity = nil
      else
        @seo_entity = SeoEntity.find_by_url(url) unless @seo_entity.nil? && defined?(@seo_entity)
      end
    end

    def page_title title
      @seo_entity ||= seo_entity
      if @seo_entity.nil? || @seo_entity.page_title.empty?
        content_for :title, title
        title
      else
        @seo_entity.page_title.gsub('\n', "\n")
      end
    end

    def head_title *titles
      unless titles.length == 1 || titles.length == 2
        raise ArgumentError.new("Wrong number of arguments: 1 or 2 allowed")
      end
      page_title, base_title = titles
      @seo_entity ||= seo_entity
      if @seo_entity.nil? || @seo_entity.head_title.empty?
        head_title = full_title(strip_tags(page_title), base_title)
      else
        head_title = full_title(@seo_entity.head_title)
      end
      head_title
    end

    def seo_up
      @seo_entity ||= seo_entity
      unless @seo_entity.nil? || @seo_entity.seo_up.empty?
        @seo_entity.seo_up
      end
    end

    def seo_down
      @seo_entity ||= seo_entity
      unless @seo_entity.nil? || @seo_entity.seo_down.empty?
        @seo_entity.seo_down
      end
    end

    def meta_description
      @seo_entity ||= seo_entity
      unless @seo_entity.nil? || @seo_entity.meta_description.empty?
        @seo_entity.meta_description
      end
    end

    def keywords
      @seo_entity ||= seo_entity
      unless @seo_entity.nil? || @seo_entity.keywords.empty?
        @seo_entity.keywords
      end
    end

    def seo_entity_meta_description
      tag('meta', content: meta_description, :name => 'description') if meta_description.present?
    end

    def seo_entity_keywords
      tag('meta', content: keywords, :name => 'keywords') if keywords.present?
    end


    private

    def full_title *titles
      page_title, base_title = titles
      if base_title.blank?
        return page_title
      end
      if page_title.blank? && !base_title.blank?
        return base_title
      end
      page_title + ' | ' + base_title
    end

  end
end
