class CreateSeoEntity < ActiveRecord::Migration
  def up
    create_table :seo_entity_seo_entities do |t|
      t.string :url
      t.string :head_title
      t.string :page_title
      t.text   :seo_up
      t.text   :seo_down
      t.text   :meta_description
      t.text   :keywords
      t.timestamps
    end
  end

  def down
    drop_table :seo_entity_seo_entities
  end
end
