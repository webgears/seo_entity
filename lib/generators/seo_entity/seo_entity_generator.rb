require 'rails/generators/active_record'

class SeoEntityGenerator < ActiveRecord::Generators::Base
  desc 'Creates migrations in application'

  argument :name, :default => "migration"
  source_root File.expand_path("../templates", __FILE__)
  include Rails::Generators::Migration

  def copy_migrations
    migration_template "create_seo_entity.rb", "db/migrate/create_seo_entity.rb"
  end

end