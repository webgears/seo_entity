if defined?(ActiveAdmin)
  ActiveAdmin.register SeoEntity::SeoEntity, :as => 'Seo Entity' do
    permit_params :url, :seo_up, :seo_down, :head_title, :page_title, :meta_description, :keywords

    index do
      column :url
      column :head_title
      column :page_title
      column :seo_up
      column :seo_down
      column :meta_description
      column :keywords
      actions
    end
  end
end