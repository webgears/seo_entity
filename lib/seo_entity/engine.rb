module SeoEntity
  class Engine < ::Rails::Engine
    isolate_namespace SeoEntity

    initializer :seo_entity do
      ActiveAdmin.application.load_paths += Dir[File.dirname(__FILE__) + '/admin']
    end

    config.to_prepare do
      ApplicationController.helper(SeoEntity::ApplicationHelper)
    end
  end
end
